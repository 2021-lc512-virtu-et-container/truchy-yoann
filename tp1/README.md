# Docker images

## Q1

Tester les commandes suivantes plusieurs fois
    
    docker run hello-world 
    docker run ubuntu date
    
et visiter https://hub.docker.com/_/hello-world, compléter le diagrame de séquence pour expliquer ce qui c'est passé depuis le lancement de la commande `docker run hello-world` jusqu'au retour dans le shell.

```mermaid
sequenceDiagram
    docker->>dockerd: Contact le daemon
    dockerd->>cache locale: Regarde si l'image demandée est présente
    dockerd->>docker: Redonne le shell au client si l'image est présente
    dockerd->>hub docker: Pull l'image demandée
    dockerd->>cache locale: Création de l'image en local
    dockerd->>docker: Redonne le shell au client
```


## Q2

À l'aide des commandes suivantes, expliquez le fonctionnement des noms des images des containers : 

    docker run registry.gitlab.com/azae/marketing/azae.net:master date
    docker run debian:sid date
    docker images

Une image docker est formatée de cette manière `<nom de l'image>:<tag>`. Un tag correspond à une version d'une image.

## Q3

À l'aide de la commande suivantes, expliquer le role et le fonctionnement du Manifest

    docker manifest inspect hello-world

Permet d'afficher la liste des manifestes d'une image. Un manifeste donne certaines informations de la version d'une image, tels que la taille, le diggest, l'architecture, et l'os.

## Q4

Charger les versions 1, latest, 1.30, uclibc, 1.31, 1-uclibc de l'image `busybox` en utilisant les IDs de layers, en déduire comment sont construite les images dockers.

## Q5

Expliquer les différences entre images et containers.

## Q6

`docker run -it debian bash` Que fait cette commande, à quoi servent les option `-i` et `-t`

## Q7 

Lancer un shell dans un container debian puis lancer la commande `date > /etc/now`. Quittez le container puis relancer un container debian pour aller regarder le contenu du fichier `/etc/now`

## Q8

Créez un dossier helloworld, y créer 2 fichier

`hello.c`

```c
#include <stdio.h>

int main() {
   printf("Hello World!");
   return 0;
}
```

et `Dockerfile`

```dockerfile
FROM scratch
COPY hello /
CMD ["/hello"]
```

compiler ce magnifique programme avec la commande `gcc hello.c -o hello` vérifiez la bonne execution du programme, puis générer l'image, la tagger en version 1.0 et la lancer.

Quelles sont les commandes que vous avez utilisé.
Corriger le container pour qu'un run lance notre programme `hello` sans erreur.

## Q9

Modifiez votre `Dockerfile` pour partir d'une distribution debian

```dockerfile
FROM debian
COPY hello /
CMD ["/hello"]
```

générer l'image, la tagger en version 2.0

à l'aide de la commande `docker history` analyser les différences entre nos versions de notre images. 

* Que représentent les ids ?
* Comment fonctionnent les layers ?
* Modifier notre programme pour afficher autre chose, puis construire une version 3 de notre container. Qu'observez-vous dans les layers ?


## Q10

Accéder à un shell dans un conteneur basé sur l’image `busybox` puis dans l’image `ubuntu`
* Lister les variables d’environnement (`env`)
* Lister les processus et leur PID (`ps`)
* Lister les interfaces réseaux (`ip`)

